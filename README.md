# Agave Sample OAuth Application #

This is a sample application, written in Django, that exercises the OAuth 2 Authorization Code grant flow against an Agave (http://agaveapi.co/) OAuth server. The application will walk the authorization code grant flow and display the access and refresh tokens obtained.

To configure the sample application simply update the views.py module with the following settings:

1. OAUTH_BASE - base url of the Agave OAuth server (also called the 'tenant url' in the Agave documentation).
2. CLIENT_ID - id of a valid Agave OAuth client. 
3. CLIENT_PWD - secret corresponding to the client above.

Example settings are contained in the views.py module that ships with the repo.


Next, install the application requirements using pip. This can be done within a virtualenv if you wish:
```
#!bash
pip install -r requirements.txt 
```

Run the Django development server on port 8000:
```
#!bash
python manage.py runserver
```

Navigate to http://localhost:8000 to see the example application. The example application has two users in its sqlite database: admin (the django admin user) and jsmith. Authenticate with either user to begin walking the OAuth flow. On the Agave Authorization site, be sure to authenticate with valid user credentials for that tenant.

The sample application stores data in a sqlite database. You can update those data using the Django admin application by navigating to http://localhost:8000/admin and authenticating with the admin credentials (by default, 'admin', 'admin').

