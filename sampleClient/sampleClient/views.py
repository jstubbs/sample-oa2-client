'''
Views for an example OAuth2 client application.  This application interacts 
with an Agave OAuth2 server to request authorization to make Agave API calls on a user's behalf.  It
then displays the relevant oauth tokens.

This application implements the OAuth2 authorization grant flow.  

Created on Jul 18, 2013
@author: jstubbs

'''

import logging
import random
import requests

from django.views.decorators.http import require_GET, require_http_methods
from django.contrib.auth import authenticate, login
from django.core.urlresolvers import reverse
from django.core.context_processors import csrf
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseRedirect
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext

from sampleClient.models import OauthAccess


# Get an instance of a logger
logger = logging.getLogger(__name__)

USERS = {'jstubbs': {'name': 'Joe Stubbs','token': 123,},
         'rion': {'name': 'Rion Dooley','nonce': '314497757'},
         'admin':{'name':'Admin', 'nonce': '123'}
        }

# -----------------------
# Authz server constants
# -----------------------
# OAUTH_BASE = 'https://agave-staging.tacc.utexas.edu/oauth2/'

AUTH_ENDPOINT = 'authorize'
NEW_TOKEN_ENDPOINT = 'token'


# --------------
# App constants
# --------------

# these work for dev.agave...:
OAUTH_BASE = 'https://dev.agave.tacc.utexas.edu/oauth2/'
APP_BASE = 'http://localhost:8000'
CLIENT_ID = 'DlwNnLu0CLg1PqfSjoTqaVYGlA0a'
CLIENT_PWD = 'hNEaPqrktSftZtymiSUS7QzLkX0a'

# django admin account:
# username: admin
# password: admin

# sample user account:
# username: jsmith
# password: abcde


class Error(Exception):
    def __init__(self, message=None):
        self.message = message
        if message:
            logger.info("Error raised:" + message)

class AccessExpiredError(Error):
    """
    Thrown when both access token and refresh token have expired.
    """
    pass


# ------
# Views
# ------

@require_http_methods(["GET", "POST"])
def login_view(request):
    """
    Login page for the sample application.
    """
    c = {}
    c.update(csrf(request))
    if request.method == 'GET':
        return render_to_response('login.html', c)
    
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            # Redirect to home page on success:
            return HttpResponseRedirect(reverse('home'))
        else:
            # Return a 'disabled account' error message
            return HttpResponseRedirect(reverse('login_error'))
    else:
        # Return an 'invalid login' error message.
        return HttpResponseRedirect(reverse('login_error'))


def logout_view(request):
    logout(request)
    return render_to_response('logout.html')

@require_GET
def login_error(request):
    logout(request)
    return render_to_response('error.html')


@require_GET
@login_required
def request_access(request):
    """
    Asks user for access to Agave.  Provides link to initiate the
    authorization grant flow via OA2 server.
    """
    user=request.user
    username = user.username
    
    # build redirect URL using requests library
    url = OAUTH_BASE + AUTH_ENDPOINT # base url
    nonce = str(random.randrange(0, 999999999))
    state = username + 'nonce:' + nonce
    
    data = {'client_id' : CLIENT_ID,
            'redirect_uri': APP_BASE + reverse("token"),
            'scope': 'PRODUCTION',
            'response_type': 'code',}
    oauth_url = requests.Request('GET', url, params=data).prepare()
    context = {'username':username,
               'oauth_url':oauth_url.url, }
    try:
        p = user.oauthaccess
    except:
        p = OauthAccess(user=user, granted_access=False,
                        access_token='', nonce=nonce)        
    else:
        p.nonce = nonce
    p.save()
    return render_to_response('request_access.html',
                              RequestContext(request, context))


@require_GET
@login_required
def home(request):
    """
    Home page for the sample application.
    """
    user = request.user
    logger.debug("User:" + str(user) + " logged in, accessing home.")
    try:
        p = user.oauthaccess
    except:
        # users will not have an oauthaccess record on first visit:
        return HttpResponseRedirect(reverse('request_access'))
    if not p or not p.granted_access:
        return HttpResponseRedirect(reverse('request_access'))
    return render_to_response('welcome.html',
                              RequestContext(request,
                                             {'access_token': p.access_token,
                                              'refresh_token': getattr(p, 'refresh_token', 'None'),
                                              'username':user.username}))

@require_GET
def oa_token(request):
    """
    Processes authorization codes and requests access tokens.
    """
    auth_code = request.GET.get('code')
    if not auth_code:
        return HttpResponseBadRequest("Authorization code missing")
    
    user = request.user
    r = get_access_token_response(auth_code=auth_code)
    logger.info("response from WSO2: " + r.content)
    if not r.status_code == 200 or not r.json():
        return HttpResponseBadRequest("Error getting token; Status code: " 
                                + str(r.status_code) + ' Content:' + r.content)

    update_oauth_access(user, r)
    return HttpResponseRedirect(reverse('home'))

        
# --------
# Helpers
# --------
def get_access_token_response(auth_code=None, refresh_token=None):
    """
    Makes a request to the OAuth server for an access token. Must pass either
    the auth_code or the refresh_token.
    
    Returns the json response.
    """
    if not auth_code and not refresh_token:
        raise Error("Must pass either an auth_code or refresh_token.")
    
    url = OAUTH_BASE + NEW_TOKEN_ENDPOINT
    data = {'redirect_uri': APP_BASE + reverse("token")}
    if auth_code:
        data['code'] = auth_code,
        data['grant_type'] = 'authorization_code'
    else:
        data['refresh_token'] = refresh_token
        data['grant_type'] = 'refresh_token'
    logger.debug("attempting to refresh access token with refresh token:" + 
                 str(refresh_token))
    logger.debug("data:" + str(data))
    return requests.post(url, auth=(CLIENT_ID, CLIENT_PWD), data=data, verify=False)

def update_oauth_access(user, response):
    """
    Updates the oauthaccess object for a user using the response object from
    a successful call to the OA2 server.
    """
    p = user.oauthaccess
    p.access_token = response.json().get('access_token')
    # refresh tokens are only issued when accessing as access token the first 
    # time; don't want to try and update on subsequent attempts.
    if response.json().get('refresh_token'):
        p.refresh_token = response.json().get('refresh_token')
    p.granted_access = True
    p.save()

    
    
