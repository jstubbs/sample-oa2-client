from django.conf.urls import patterns, include, url
import views
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    # main page of app, with userid
    url(r'^home/$', views.home, name = 'home'),    
    
    # request access splash page
    url(r'^request_access/$', views.request_access, name = 'request_access'),
    
    # processes auth code from OAuth2 server and requests access token
    url(r'^oauth2/token', views.oa_token, name = 'token'),

    # login
    url(r'^login/error', views.login_error, name='login_error'),
    url(r'^login', views.login_view, name='login'),
    url(r'^logout', views.logout_view, name='logout'),
    
    # admin
    (r'^admin/', include(admin.site.urls)),

    # catch all to go to the home page as a last resort
    url(r'^$', views.home, name = 'home'),
) 
