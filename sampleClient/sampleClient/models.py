from django.db import models
from django.contrib.auth.models import User

class OauthAccess(models.Model):
    """
    OAuth access data.
    """
    user = models.OneToOneField(User, primary_key=True)
    granted_access = models.BooleanField()
    access_token = models.CharField(max_length=256)
    nonce = models.CharField(max_length=256)
    refresh_token = models.CharField(max_length=256)