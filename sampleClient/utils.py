"""
Utilities for exploring the opanAM api.
"""

import requests

ACCESS_TOKEN_URL = 'http://openam.example.com:8080/openam/oauth2/access_token' 
TOKEN_INFO_URL = 'http://openam.example.com:8080/openam/oauth2/tokeninfo'

AUTHN_TOKEN_URL = 'http://openam.example.com:8080/openam/identity/authenticate'
CHECK_AUTHN_TOKEN_URL = 'http://openam.example.com:8080/openam/identity/isTokenValid'


def get_authn_token():
#     r = requests.post(AUTHN_TOKEN_URL,
#                       auth=('sampleDjangoClient','p@ssw0rd'))
    r = requests.post(AUTHN_TOKEN_URL, 
                      data={'username':'sampleDjangoClient',
                            'password':'p@ssw0rd'})
    authn_token = r.content[len('token.id='):].strip('\n')
    return authn_token, r

def get_access_token():
    """
    Uses client credentials grant type to obtain an access token.
    """
    r = requests.post(ACCESS_TOKEN_URL, 
                      auth=('sampleDjangoClient','p@ssw0rd'), 
                      data={'grant_type':'client_credentials', 
                            'scope':'photo',})
    
    return r.json().get('access_token'), r

def get_access_token_auth_code(auth_code):
    """
    Uses authorization code grant type to obtain an access token.
    """
    r = requests.post(ACCESS_TOKEN_URL, 
                      auth=('sampleDjangoClient','p@ssw0rd'), 
                      data={'grant_type':'authorization', 
                            'scope':'photo',})
    
    return r.json().get('access_token'), r

def get_access_token_with_sesion(authn_token):
    """
    Uses client credentials grant type to obtain an access token authenticating
    with a previously established OpenAM session.
    """
    r = requests.post(ACCESS_TOKEN_URL, 
                      headers={'iPlanetDirectoryPro' : authn_token}, 
                      data={'grant_type':'client_credentials', 
                            'scope':'photo',})
    
    return r.json().get('access_token'), r

def get_pwd_access_token():
    """
    Uses password grant type to obtain an access token.
    """
    r = requests.post()
    r = requests.post(ACCESS_TOKEN_URL, 
                      auth=('sampleDjangoClient','p@ssw0rd'), 
                      data={'grant_type':'password', 
                            'scope':'photo cn',
                            'username':'amAdmin',
                            'password':'d3f@ult$'})
    
    return r.json().get('access_token'), r


def check_authn_token(token):
    url = CHECK_AUTHN_TOKEN_URL
    data = {'tokenid' : token}
    r = requests.post(url, params=data)
    return r

def check_access_token(token):
    url = TOKEN_INFO_URL
    data = {'access_token' : token}
    r = requests.get(url, params=data)
    if not r.status_code == requests.codes.ok:
        print "Token not valid -- status code=", r.status_code, r.content
    return r


